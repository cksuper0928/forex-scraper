from scrappers import scrapper_saxo_3_25_delta as Saxo_3_25_delta

saxo_3_atm_url = "https://fxowebtools.saxobank.com/otc.html"

bot = Saxo_3_25_delta.Saxo_3_25_delta_scrapper()
bot.connect_mysql()
bot.get_status()
page_soap = bot.fetch_soup(saxo_3_atm_url)
pairs = bot.get_pairs(page_soap)
bot.insert_pairs(pairs)