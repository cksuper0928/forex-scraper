import json
import mysql.connector
from datetime import date, timedelta, datetime
import csv
import requests
import time
import random
from bs4 import BeautifulSoup as BS

class Saxo_3_25_delta_scrapper:
    def __init__(self):
        self.today = date.today()
        self.date=self.today.strftime("%b%d%Y")
        self.now = datetime.now()
        self.datetime=self.now.strftime("%Y-%m-%d %H:%M:%S")
        self.db_name = "tb_saxo_3_25_delta"
        self.status={
            "name"          : "",
            "user_access"   : "",
            "frequency"     : "",
            "status"        : ""
        }
    
    def connect_mysql(self):
        try:
            self.mydb = mysql.connector.connect(
                host="localhost",
                user="root",
                passwd="root",
                database="forex_scraping"
            ) 
            print('[+]\t CONNECTED TO DATABASE')
            self.mycursor = self.mydb.cursor()
        
        except Exception as e:
            print('[x]\t DATABASE CONNECTION ERROR ..')
            print(e)
    
    def get_status(self):
        status_list = []
        sql = "SELECT * FROM tb_scrapper WHERE tb_name='"+self.db_name+"'"
        self.mycursor.execute(sql)
        results=self.mycursor.fetchall()
        for result in results:
            for r in result:
                status_list.append(r)
        self.status["name"]         = status_list[1]
        self.status["user_access"]  = status_list[3]
        self.status["frequency"]    = status_list[4]
        self.status["status"]       = status_list[5]
    
    def fetch_soup(self,url):
        headers={'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0'}
        try:
            r=requests.get(url=url,headers=headers)
            #print(r.content)
            soup=BS(r.content,'lxml')
            return(soup)
        except Exception as e:
            print(e)
    
    def get_pairs(self,soup):
        pair_list = []
        d_time=soup.find('div',{'class':'blogTitle'}).getText()
        d_time=d_time.replace("\r\n", "")
        d_time=d_time.replace("   ", "")
        d_time_split = d_time.split(",")

        d_time_date = d_time_split[0]
        d_time_date_split = d_time_date.split(" ")
        d_time_date = d_time_date_split[0]+"/"+datetime.strptime('Dec', '%b').strftime('%m')+"/"+d_time_date_split[2]
        d_time_date = d_time_date.replace("\t\t", "")

        d_time_time = d_time_split[1][0:3]+":"+d_time_split[1][3:5]+":00"

        tables = soup.find_all('table', {'class':'volTable'})
        table = tables[1]
        pairs = table.find_all('tr')

        for pair in pairs:
            if pair == pairs[0]:
                continue

            fields = pair.find_all('td')

            pair = fields[1].getText()
            pair_id = self.get_pair_id(pair)

            spot = fields[2].getText()
            spot_split = spot.split("(")
            field_1w = fields[3].getText()
            field_1w_split = field_1w.split("(")
            field_1m = fields[4].getText()
            field_1m_split = field_1m.split("(")
            field_3m = fields[5].getText()
            field_3m_split = field_3m.split("(")
            field_6m = fields[6].getText()
            field_6m_split = field_6m.split("(")
            field_9m = fields[7].getText()
            field_9m_split = field_9m.split("(")
            field_1y = fields[8].getText()
            field_1y_split = field_1y.split("(")
            pair_info = {
                "pair"      :   str(pair_id),
                "spot"      :   spot_split[0],
                "1w"        :   field_1w_split[0],
                "1m"        :   field_1m_split[0],
                "3m"        :   field_3m_split[0],
                "6m"        :   field_6m_split[0],
                "9m"        :   field_9m_split[0],
                "1y"        :   field_1y_split[0],
                "date"      :   d_time_date,
                "time"      :   d_time_time
            }
            pair_list.append(pair_info)
            
        return pair_list
    
    def get_pair_name(self, pair_id):
        pair_sql = "SELECT " + self.db_name + " FROM tb_instrument WHERE pair_id='" + pair_id + "'"
        self.mycursor.execute(pair_sql)
        results=self.mycursor.fetchall()
        return results[0][0]
    
    def get_pair_id(self, pair_name):
        pair_sql = "SELECT pair_id FROM tb_instrument WHERE " + self.db_name + "='" + pair_name + "'"
        self.mycursor.execute(pair_sql)
        results=self.mycursor.fetchall()
        return results[0][0]
    
    def get_latest_cycle(self):
        sql = "SELECT latest_cycle FROM tb_scrapper WHERE tb_name='"+self.db_name+"'"
        self.mycursor.execute(sql)
        results=self.mycursor.fetchall()
        return results[0][0]

    def get_forex_data(self):
        latest_cycle_num = self.get_latest_cycle()
        sql = "SELECT * FROM " + self.db_name +" WHERE cycle_num='"+str(latest_cycle_num)+"'"
        self.mycursor.execute(sql)
        results=self.mycursor.fetchall()
        return results
    
    def insert_pairs(self, pairs):
        latest_cycle_num = self.get_latest_cycle()
        new_cycle_num = latest_cycle_num + 1

        # Update scrapper table
        scrapper_sql = "UPDATE tb_scrapper SET latest_cycle=" + str(new_cycle_num) + " WHERE tb_name='" + self.db_name + "'"
        self.mycursor.execute(scrapper_sql)
        self.mydb.commit()

        # Insert Pairs to Table
        for pair in pairs:
            try:
                pair_sql = "INSERT INTO " + self.db_name + " (pair, spot, 1w, 1m, 3m, 6m, 9m, 1y, date, time, cycle_num) " + "VALUES " +"('" + pair["pair"] + "','" + pair["spot"] + "','" + pair["1w"] + "','" + pair["1m"] + "','" + pair["3m"] + "','" + pair["6m"] + "','" + pair["9m"] + "','" + pair["1y"] + "','" + pair["date"] + "','" + pair["time"] + "','" + str(new_cycle_num) + "')"
                self.mycursor.execute(pair_sql)
                self.mydb.commit()
            except Exception as e:
                print("[!] MySQL Error : ")
                print(e)
        
        print("[+] Pair " + str(new_cycle_num) + "Inserted !")
    

        
        



