import json
import mysql.connector
from datetime import date, timedelta, datetime
import csv
import requests
import time
import random
from bs4 import BeautifulSoup as BS

class Myfxbook_scrapper:
    def __init__(self):
        self.today = date.today()
        self.date=self.today.strftime("%b%d%Y")
        self.now = datetime.now()
        self.datetime=self.now.strftime("%Y-%m-%d %H:%M:%S")
        self.date = self.now.strftime("%d/%m/%Y")
        self.time = self.now.strftime("%H:%M:%S")
        self.db_name = "tb_myfxbook"
        self.email = 'info@forexsentiment.net'
        self.password = 'fxsentiment123456789'
        self.status={
            "name"          : "",
            "user_access"   : "",
            "frequency"     : "",
            "status"        : ""
        }
    
    def connect_mysql(self):
        try:
            self.mydb = mysql.connector.connect(
                host="localhost",
                user="root",
                passwd="root",
                database="forex_scraping"
            ) 
            print('[+]\t CONNECTED TO DATABASE')
            self.mycursor = self.mydb.cursor()
        except Exception as e:
            print('[x]\t DATABASE CONNECTION ERROR ..')
            print(e)
    
    def get_status(self):
        status_list = []
        sql = "SELECT * FROM tb_scrapper WHERE tb_name='"+self.db_name+"'"
        self.mycursor.execute(sql)
        results=self.mycursor.fetchall()
        for result in results:
            for r in result:
                status_list.append(r)
        self.status["name"]         = status_list[1]
        self.status["user_access"]  = status_list[3]
        self.status["frequency"]    = status_list[4]
        self.status["status"]       = status_list[5]
    
    def fetch_soup(self,url):
        headers={'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0'}
        try:
            r=requests.get(url=url,headers=headers)
            #print(r.content)
            soup=BS(r.content,'lxml')
            return(soup)
        except Exception as e:
            print(e)
    
    def get_pairs(self):
        # Get session id
        session_url = 'https://www.myfxbook.com/api/login.json?email='+self.email+'&password='+self.password
        session_reponse = requests.get(session_url)
        session_id = session_reponse.json()['session']

        # Get Community Outlook.
        url = 'https://www.myfxbook.com/api/get-community-outlook.json?session='+session_id
        response = requests.get(url)
        if response.status_code == 200:
            data = response.json()

        return data["symbols"]
    
    def get_outlook_by_country(self, symbol_name):
        # Get session id
        session_url = 'https://www.myfxbook.com/api/login.json?email='+self.email+'&password='+self.password
        session_reponse = requests.get(session_url)
        session_id = session_reponse.json()['session']

        url = 'https://www.myfxbook.com/api/get-community-outlook-by-country.json?session='+session_id+'&symbol='+symbol_name
        response = requests.get(url)
        if response.status_code == 200:
            data = response.json()
        
        str_d = ""
        for d in data["countries"]:
            str_d += json.dumps(d)+"&"

        return data["countries"]
    
    def get_pair_name(self, pair_id):
        pair_sql = "SELECT " + self.db_name + " FROM tb_instrument WHERE pair_id='" + pair_id + "'"
        self.mycursor.execute(pair_sql)
        results=self.mycursor.fetchall()
        return results[0][0]
    
    def get_pair_id(self, pair_name):
        pair_sql = "SELECT pair_id FROM tb_instrument WHERE " + self.db_name + "='" + pair_name + "'"
        self.mycursor.execute(pair_sql)
        results=self.mycursor.fetchall()
        return results[0][0]
    
    def get_latest_cycle(self):
        sql = "SELECT latest_cycle FROM tb_scrapper WHERE tb_name='"+self.db_name+"'"
        self.mycursor.execute(sql)
        results=self.mycursor.fetchall()
        return results[0][0]

    def get_forex_data(self):
        latest_cycle_num = self.get_latest_cycle()
        sql = "SELECT * FROM " + self.db_name +" WHERE cycle_num='"+str(latest_cycle_num)+"'"
        self.mycursor.execute(sql)
        results=self.mycursor.fetchall()
        return results
    
    def insert_pairs(self, pairs):
        latest_cycle_num = self.get_latest_cycle()
        new_cycle_num = latest_cycle_num + 1

        # Update scrapper table
        scrapper_sql = "UPDATE tb_scrapper SET latest_cycle=" + str(new_cycle_num) + " WHERE tb_name='" + self.db_name + "'"
        self.mycursor.execute(scrapper_sql)
        self.mydb.commit()

        instrumentss_list = ['EURUSD', 'GBPUSD', 'USDJPY', 'GBPJPY', 'USDCAD', 'EURAUD', 'EURJPY', 'AUDCAD', 'AUDJPY', 'AUDNZD', 'AUDUSD', 'CADJPY', 'EURCAD', 'EURCHF', 'EURGBP', 'EURNOK', 'EURNZD', 'EURSEK', 'EURTRY', 'GBPCAD', 'GBPCHF', 'NZDCAD', 'NZDJPY', 'NZDUSD', 'USDCHF', 'USDNOK', 'USDSEK', 'USDSGD', 'USDTRY', 'USDZAR', 'CHFJPY', 'AUDCHF', 'GBPNZD', 'NZDCHF', 'XAGUSD', 'XAUUSD', 'CADCHF', 'GBPAUD', 'SPA35', 'US30', 'EURZAR', 'US500', 'UK100', 'NAS100', 'US2000', 'FRA40', 'GER30', 'AUS200', 'JPN225', 'USDCNH', 'IT40', 'XTIUSD', 'XBRUSD', 'HK50']
        
        # Insert Pairs to Table
        for pair in pairs:
            if pair["name"] in instrumentss_list:
                try:
                    countries = []
                    pair_id = self.get_pair_id(pair["name"])
                    countries = self.get_outlook_by_country(pair["name"])

                    pair_sql = "INSERT INTO " + self.db_name + " (name, shortPercentage, longPercentage, shortVolume, longVolume, longPositions, shortPositions, totalPositions, avgShortPrice, avgLongPrice, date, time, cycle_num) " + "VALUES " +"('" + str(pair_id) + "','" + str(pair["shortPercentage"]) + "','" + str(pair["longPercentage"]) + "','" + str(pair["shortVolume"]) + "','" + str(pair["longVolume"]) + "','" + str(pair["longPositions"]) + "','" + str(pair["shortPositions"]) + "','" + str(pair["totalPositions"]) + "','" + str(pair["avgShortPrice"]) + "','" + str(pair["avgLongPrice"]) + "','" + self.date + "','" + self.time + "','" + str(new_cycle_num) + "')"
                    self.mycursor.execute(pair_sql)
                    self.mydb.commit()

                    if countries != []:
                        for country in countries:
                            try:
                                country_sql = "INSERT INTO tb_myfxbook_by_country (pair_id, country_code, long_volume, short_volume, long_positions, short_positions, date, time, cycle_num) " + "VALUES " +"('" + str(pair_id) + "','" + country["code"] + "','" + str(country["longVolume"]) + "','" + str(country["shortVolume"]) + "','" + str(country["longPositions"]) + "','" + str(country["shortPositions"]) + "','" + self.date + "','" + self.time + "','" + str(new_cycle_num) + "')"
                                self.mycursor.execute(country_sql)
                                self.mydb.commit()
                            except Exception as e:
                                print("[!] MySQL Error : " + country["code"])
                                print(e)

                except Exception as e:
                    print("[!] MySQL Error : " + pair["name"])
                    print(e)
        
        print("[+] Pair " + str(new_cycle_num) + "Inserted !")
    

        
        



