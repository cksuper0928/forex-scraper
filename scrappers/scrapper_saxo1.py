import json
import mysql.connector
from datetime import date, timedelta, datetime
import csv
import requests
import time
import random
from pytz import timezone
import pytz
from bs4 import BeautifulSoup as BS

class Saxo_1_scopy_scrapper:
    def __init__(self):
        self.today = date.today()
        self.date=self.today.strftime("%b%d%Y")
        self.now = datetime.now()
        self.datetime=self.now.strftime("%Y-%m-%d %H:%M:%S")
        self.db_name = "tb_saxo_1"
        self.api_url = 'https://fxlabs.iitech.dk/Api/DataProviderApi.svc/OP/GetAggregatedRatiosData?il=EURUSD;EURCHF;XAUUSD;GBPUSD;USDJPY;AUDUSD;EURJPY;USDCHF;EURGBP;GBPJPY&itr=60&ifc=true'
        self.config_url = 'https://c.go-mpulse.net/api/config.json?key=34U7S-SSRUB-7SUUE-R4ZBR-WND93&d=www.home.saxo&t=5256542&v=1.571.0&if=&sl=1&si=24bb6d54-b097-4826-95d5-d8b9b817da4e-q2vq1x&bcn=%2F%2F60062f0f.akstat.io%2F&plugins=AK,ConfigOverride,Continuity,PageParams,IFrameDelay,AutoXHR,SPA,Angular,Backbone,Ember,History,RT,CrossDomain,BW,PaintTiming,NavigationTiming,ResourceTiming,Memory,CACHE_RELOAD,Errors,TPAnalytics,UserTiming,LOGN&acao='
        self.status={
            "name"          : "",
            "user_access"   : "",
            "frequency"     : "",
            "status"        : ""
        }
    
    def connect_mysql(self):
        try:
            self.mydb = mysql.connector.connect(
                host="localhost",
                user="root",
                passwd="root",
                database="forex_scraping"
            ) 
            print('[+]\t CONNECTED TO DATABASE')
            self.mycursor = self.mydb.cursor()
        except Exception as e:
            print('[x]\t DATABASE CONNECTION ERROR ..')
            print(e)
    
    def get_status(self):
        status_list = []
        sql = "SELECT * FROM tb_scrapper WHERE tb_name='"+self.db_name+"'"
        self.mycursor.execute(sql)
        results=self.mycursor.fetchall()
        for result in results:
            for r in result:
                status_list.append(r)
        self.status["name"]         = status_list[1]
        self.status["user_access"]  = status_list[3]
        self.status["frequency"]    = status_list[4]
        self.status["status"]       = status_list[5]
    
    def fetch_soup(self,url):
        url = self.api_url
        headers={'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0'}
        try:
            r=requests.get(url=url,headers=headers)
            #print(r.content)
            soup=BS(r.content,'lxml')
            return(soup)
        except Exception as e:
            print(e)
    
    def get_pairs(self):
        details = []
        # Get config
        config_url =  self.config_url
        c_response = requests.get(config_url)
        if c_response.status_code == 200:
            c_data = c_response.json()
            c_d_time = c_data["h.t"]
            c_d_time = str(c_d_time)
            c_d_time = c_d_time.replace("/", "")
            c_d_time = c_d_time.replace("Date", "")
            c_d_time = c_d_time.replace("(", "")
            c_d_time = c_d_time.replace(")", "")
            c_d_time = c_d_time[0:10]

            fmt = '%d %b %Y %H:%M'
            utc = pytz.utc
            utc_dt = utc.localize(datetime.utcfromtimestamp(int(c_d_time)))
            c_d_time = utc_dt.strftime(fmt)


        url = self.api_url
        response = requests.get(url, params={"cd" : c_d_time})
        if response.status_code == 200:
            data = response.json()
            d_time = data["DataDateTime"]
            d_time = d_time.replace("/", "")
            d_time = d_time.replace("Date", "")
            d_time = d_time.replace("(", "")
            d_time = d_time.replace(")", "")
            d_time = d_time[0:10]

            fmt = '%Y-%m-%d %H:%M:%S %Z%z'
            utc = pytz.utc
            utc_dt = utc.localize(datetime.utcfromtimestamp(int(d_time)))
            d_time = utc_dt.strftime(fmt)
            d_time_date = utc_dt.strftime("%d/%m/%Y")
            d_time_time = utc_dt.strftime("%H:%M:%S")

            for d in data["PositionRatioData"]:
                pair_id = self.get_pair_id(d["InstrumentName"])
                long_ratio = d["LongPosition"]/(d["LongPosition"]+d["ShortPosition"])*100
                long_ratio = "%.2f" % long_ratio
                short_ratio = d["ShortPosition"]/(d["LongPosition"]+d["ShortPosition"])*100
                short_ratio = "%.2f" % short_ratio
                details.append(
                    {
                        'pair': str(pair_id),
                        'long': long_ratio,
                        'short': short_ratio,
                        'date': d_time_date,
                        'time': d_time_time
                    }
                )

        return details
    
    def get_pair_name(self, pair_id):
        pair_sql = "SELECT " + self.db_name + " FROM tb_instrument WHERE pair_id='" + pair_id + "'"
        self.mycursor.execute(pair_sql)
        results=self.mycursor.fetchall()
        return results[0][0]
    
    def get_pair_id(self, pair_name):
        pair_sql = "SELECT pair_id FROM tb_instrument WHERE " + self.db_name + "='" + pair_name + "'"
        self.mycursor.execute(pair_sql)
        results=self.mycursor.fetchall()
        return results[0][0]
    
    def get_latest_cycle(self):
        sql = "SELECT latest_cycle FROM tb_scrapper WHERE tb_name='"+self.db_name+"'"
        self.mycursor.execute(sql)
        results=self.mycursor.fetchall()
        return results[0][0]

    def get_forex_data(self):
        latest_cycle_num = self.get_latest_cycle()
        sql = "SELECT * FROM " + self.db_name +" WHERE cycle_num='"+str(latest_cycle_num)+"'"
        self.mycursor.execute(sql)
        results=self.mycursor.fetchall()
        return results
    
    def insert_pairs(self, pairs):
        latest_cycle_num = self.get_latest_cycle()
        new_cycle_num = latest_cycle_num + 1

        # Update scrapper table
        scrapper_sql = "UPDATE tb_scrapper SET latest_cycle=" + str(new_cycle_num) + " WHERE tb_name='" + self.db_name + "'"
        self.mycursor.execute(scrapper_sql)
        self.mydb.commit()

        # Insert Pairs to Table
        for pair in pairs:
            try:
                pair_sql = "INSERT INTO " + self.db_name + " (pair, pair_long, pair_short, date, time, cycle_num) " + "VALUES " +"('" + pair["pair"] + "','" + pair["long"] + "','" + pair["short"] + "','" + pair["date"] + "','" + pair["time"] + "','" + str(new_cycle_num) + "')"
                self.mycursor.execute(pair_sql)
                self.mydb.commit()
            except Exception as e:
                print("[!] MySQL Error : ")
                print(e)
        
        print("[+] Pair " + str(new_cycle_num) + "Inserted !")
    

        
        



