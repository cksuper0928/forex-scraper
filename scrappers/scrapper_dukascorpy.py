import json
import mysql.connector
from datetime import date, timedelta, datetime
import csv
import requests
import time
import random
from bs4 import BeautifulSoup as BS
from pytz import timezone
import pytz

class Dukascopy_scrapper:
    def __init__(self):
        self.today = date.today()
        self.date=self.today.strftime("%b%d%Y")
        self.now = datetime.now()
        self.datetime=self.now.strftime("%Y-%m-%d %H:%M:%S")
        self.db_name = "tb_dukascopy"
        self.api_key = 'odkh5fclm8000000'
        self.api_url = 'https://freeserv.dukascopy.com/2.0/?path=api/instrumentList&key='
        self.status={
            "name"          : "",
            "user_access"   : "",
            "frequency"     : "",
            "status"        : ""
        }
    
    def connect_mysql(self):
        try:
            self.mydb = mysql.connector.connect(
                host="localhost",
                user="root",
                passwd="root",
                database="forex_scraping"
            ) 
            print('[+]\t CONNECTED TO DATABASE')
            self.mycursor = self.mydb.cursor()
        except Exception as e:
            print('[x]\t DATABASE CONNECTION ERROR ..')
            print(e)
    
    def get_status(self):
        status_list = []
        sql = "SELECT * FROM tb_scrapper WHERE tb_name='"+self.db_name+"'"
        self.mycursor.execute(sql)
        results=self.mycursor.fetchall()
        for result in results:
            for r in result:
                status_list.append(r)
        self.status["name"]         = status_list[1]
        self.status["user_access"]  = status_list[3]
        self.status["frequency"]    = status_list[4]
        self.status["status"]       = status_list[5]
    
    def fetch_soup(self,url):
        headers={'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0'}
        try:
            r=requests.get(url=url,headers=headers)
            #print(r.content)
            soup=BS(r.content,'lxml')
            return(soup)
        except Exception as e:
            print(e)
    
    def get_pairs(self, params, url):
        details = []
        response = requests.get(url, timeout = 10, proxies = None, params=params)
        if response.status_code == 200:
            data = response.json()
            instruments = ['BRENT.CMD/USD', 'NZD/USD', 'AUD/USD', 'GBP/USD', 'USD/CAD', 'XAU/USD', 'EUR/USD', 'USD/CHF', 'EUR/JPY', 'USD/JPY']

            for d in data:
                if d["title"] in instruments:
                    d_time = d["date"]
                    d_time = d_time.replace("/", "")
                    d_time = d_time.replace("Date", "")
                    d_time = d_time.replace("(", "")
                    d_time = d_time.replace(")", "")
                    d_time = d_time[0:10]

                    fmt = '%Y-%m-%d %H:%M:%S %Z%z'
                    utc = pytz.utc
                    utc_dt = utc.localize(datetime.utcfromtimestamp(int(d_time)))
                    d_time = utc_dt.strftime(fmt)

                    d_time_split = d_time.split(" ")
                    d_date = d_time_split[0]
                    d_date_split = d_date.split("-")
                    d_date = d_date_split[2]+"/"+d_date_split[1]+"/"+d_date_split[0]

                    d_time_str = d_time_split[1]

                    pair_id = self.get_pair_id(d["title"].replace("/", ""))

                    details.append(
                        {
                            'pair'  : str(pair_id),
                            'long'  : d["long"],
                            'short' : d["short"],
                            'date'  : d_date,
                            'time'  : d_time_str,
                        }
                    )

        return details
    
    def get_pair_name(self, pair_id):
        pair_sql = "SELECT " + self.db_name + " FROM tb_instrument WHERE pair_id='" + pair_id + "'"
        self.mycursor.execute(pair_sql)
        results=self.mycursor.fetchall()
        return results[0][0]
    
    def get_pair_id(self, pair_name):
        pair_sql = "SELECT pair_id FROM tb_instrument WHERE " + self.db_name + "='" + pair_name + "'"
        self.mycursor.execute(pair_sql)
        results=self.mycursor.fetchall()
        return results[0][0]
    
    def get_latest_cycle(self):
        sql = "SELECT latest_cycle FROM tb_scrapper WHERE tb_name='"+self.db_name+"'"
        self.mycursor.execute(sql)
        results=self.mycursor.fetchall()
        return results[0][0]

    def get_forex_data(self):
        latest_cycle_num = self.get_latest_cycle()
        sql = "SELECT * FROM " + self.db_name +" WHERE cycle_num='"+str(latest_cycle_num)+"'"
        self.mycursor.execute(sql)
        results=self.mycursor.fetchall()
        return results
    
    def insert_pairs(self, pairs):
        latest_cycle_num = self.get_latest_cycle()
        new_cycle_num = latest_cycle_num + 1

        # Update scrapper table
        scrapper_sql = "UPDATE tb_scrapper SET latest_cycle=" + str(new_cycle_num) + " WHERE tb_name='" + self.db_name + "'"
        self.mycursor.execute(scrapper_sql)
        self.mydb.commit()

        # Insert Pairs to Table
        for pair in pairs:
            try:
                pair_sql = "INSERT INTO " + self.db_name + " (pair, pair_long, pair_short, date, time, cycle_num) " + "VALUES " +"('" + pair["pair"] + "','" + pair["long"] + "','" + pair["short"] + "','" + pair["date"] + "','" + pair["time"] + "','" + str(new_cycle_num) + "')"
                self.mycursor.execute(pair_sql)
                self.mydb.commit()
            except Exception as e:
                print("[!] MySQL Error : ")
                print(e)
        
        print("[+] Pair " + str(new_cycle_num) + "Inserted !")
    

        
        



