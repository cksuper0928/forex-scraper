from flask import Flask, render_template, session, request, copy_current_request_context
from flask_socketio import SocketIO, emit, join_room, leave_room, close_room, rooms, disconnect
from threading import Lock
import random
import time
import mysql.connector
from scrappers import scrapper_xm as XM 
from scrappers import scrapper_dukascorpy as Dukascopy
from scrappers import scrapper_forexfactory as Forexfactory
from scrappers import scrapper_tradecaptain as Tradecaptain
from scrappers import scrapper_dailyfx as Dailyfx
from scrappers import scrapper_myfxbook as Myfxbook
from scrappers import scrapper_saxo1 as Saxo_1
from scrappers import scrapper_saxo2 as Saxo_2
from scrappers import scrapper_saxo_3_atm as Saxo_3_atm
from scrappers import scrapper_saxo_3_25_delta as Saxo_3_25_delta
from scrappers import scrapper_oanda as Oanda
from crontab import CronTab
import json
import cron

async_mode = None

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread_xm = None
thread_dukascopy = None
thread_scrapper = None
thread_forexfactory = None
thread_tradecaptain = None
thread_dailyfx = None
thread_myfxbook = None
thread_saxo_1 = None
thread_saxo_2 = None
thread_saxo_3_atm = None
thread_saxo_3_25_delta = None
thread_oanda = None
thread_lock = Lock()

@app.route('/')
def index():
    return render_template('index.html', async_mode=socketio.async_mode)

@app.route('/scrappers')
def scrappers():
    return render_template('scrapper.html', async_mode=socketio.async_mode)

@app.route('/start_xm')
def start_xm():
    cron_manager = cron.Cron_scrapper()
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="forex_scraping"
    )
    print('[+]\t CONNECTED TO DATABASE')
    mycursor = mydb.cursor()
    sql = "UPDATE tb_scrapper SET status=1 WHERE tb_name='tb_xm'"
    mycursor.execute(sql)
    mydb.commit()
    # Start xm scraper
    try:
        cron_manager.xm_cron_start()
    except Exception as e:
        print(e)
    
    # xm_tab = CronTab(tabfile='xm_cron.tab')
    # for result in xm_tab.run_scheduler():
    #     print("XM Cron is running")
    
    return render_template('scrapper.html', async_mode=socketio.async_mode)

@app.route('/start_dukascopy')
def start_dukascopy():
    cron_manager = cron.Cron_scrapper()
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="forex_scraping"
    )
    print('[+]\t CONNECTED TO DATABASE')
    mycursor = mydb.cursor()
    sql = "UPDATE tb_scrapper SET status=1 WHERE tb_name='tb_dukascopy'"
    mycursor.execute(sql)
    mydb.commit()
    # Start dukascopy_cron scraper
    try:
        cron_manager.dukascopy_cron_start()
    except Exception as e:
        print(e)

    # dukascopy_tab = CronTab(tabfile='dukascopy_cron.tab')
    # for result in dukascopy_tab.run_scheduler():
    #     print("Dukascopy Cron is running")
    
    return render_template('scrapper.html', async_mode=socketio.async_mode)

@app.route('/start_forexfactory')
def start_forexfactory():
    cron_manager = cron.Cron_scrapper()
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="forex_scraping"
    )
    print('[+]\t CONNECTED TO DATABASE')
    mycursor = mydb.cursor()
    sql = "UPDATE tb_scrapper SET status=1 WHERE tb_name='tb_forexfactory'"
    mycursor.execute(sql)
    mydb.commit()
    # Start forexfactory_cron scraper
    try:
        cron_manager.forexfactory_cron_start()
    except Exception as e:
        print(e)

    # forexfactory_tab = CronTab(tabfile='forexfactory_cron.tab')
    # for result in forexfactory_tab.run_scheduler():
    #     print("Forexfactory Cron is running")
    
    return render_template('scrapper.html', async_mode=socketio.async_mode)

@app.route('/start_tradecaptain')
def start_tradecaptain():
    cron_manager = cron.Cron_scrapper()
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="forex_scraping"
    )
    print('[+]\t CONNECTED TO DATABASE')
    mycursor = mydb.cursor()
    sql = "UPDATE tb_scrapper SET status=1 WHERE tb_name='tb_tradecaptain'"
    mycursor.execute(sql)
    mydb.commit()
    # Start Tradecaptain cron scraper
    try:
        cron_manager.tradecaptain_cron_start()
    except Exception as e:
        print(e)

    # tradecaptain_tab = CronTab(tabfile='tradecaptain_cron.tab')
    # for result in tradecaptain_tab.run_scheduler():
    #     print("Tradecaptain Cron is running")
    
    return render_template('scrapper.html', async_mode=socketio.async_mode)

@app.route('/start_dailyfx')
def start_dailyfx():
    cron_manager = cron.Cron_scrapper()
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="forex_scraping"
    )
    print('[+]\t CONNECTED TO DATABASE')
    mycursor = mydb.cursor()
    sql = "UPDATE tb_scrapper SET status=1 WHERE tb_name='tb_dailyfx'"
    mycursor.execute(sql)
    mydb.commit()
    # Start dailyfx cron scraper
    try:
        cron_manager.dailyfx_cron_start()
    except Exception as e:
        print(e)

    # dailyfx_tab = CronTab(tabfile='dailyfx_cron.tab')
    # for result in dailyfx_tab.run_scheduler():
    #     print("Dailyfx Cron is running")
    
    return render_template('scrapper.html', async_mode=socketio.async_mode)

@app.route('/start_myfxbook')
def start_myfxbook():
    cron_manager = cron.Cron_scrapper()
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="forex_scraping"
    )
    print('[+]\t CONNECTED TO DATABASE')
    mycursor = mydb.cursor()
    sql = "UPDATE tb_scrapper SET status=1 WHERE tb_name='tb_myfxbook'"
    mycursor.execute(sql)
    mydb.commit()
    
    try:
        cron_manager.myfxbook_cron_start()
    except Exception as e:
        print(e)
    
    return render_template('scrapper.html', async_mode=socketio.async_mode)

@app.route('/start_saxo_1')
def start_saxo_1():
    cron_manager = cron.Cron_scrapper()
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="forex_scraping"
    )
    print('[+]\t CONNECTED TO DATABASE')
    mycursor = mydb.cursor()
    sql = "UPDATE tb_scrapper SET status=1 WHERE tb_name='tb_saxo_1'"
    mycursor.execute(sql)
    mydb.commit()
    
    try:
        cron_manager.saxo1_cron_start()
    except Exception as e:
        print(e)
    
    return render_template('scrapper.html', async_mode=socketio.async_mode)

@app.route('/start_saxo_2')
def start_saxo_2():
    cron_manager = cron.Cron_scrapper()
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="forex_scraping"
    )
    print('[+]\t CONNECTED TO DATABASE')
    mycursor = mydb.cursor()
    sql = "UPDATE tb_scrapper SET status=1 WHERE tb_name='tb_saxo_2'"
    mycursor.execute(sql)
    mydb.commit()
    
    try:
        cron_manager.saxo2_cron_start()
    except Exception as e:
        print(e)
    
    return render_template('scrapper.html', async_mode=socketio.async_mode)

@app.route('/start_saxo_3_atm')
def start_saxo_3_atm():
    cron_manager = cron.Cron_scrapper()
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="forex_scraping"
    )
    print('[+]\t CONNECTED TO DATABASE')
    mycursor = mydb.cursor()
    sql = "UPDATE tb_scrapper SET status=1 WHERE tb_name='tb_saxo_3_atm'"
    mycursor.execute(sql)
    mydb.commit()
    
    try:
        cron_manager.saxo3_atm_cron_start()
    except Exception as e:
        print(e)
    
    return render_template('scrapper.html', async_mode=socketio.async_mode)

@app.route('/start_saxo_3_25_delta')
def start_saxo_3_25_delta():
    cron_manager = cron.Cron_scrapper()
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="forex_scraping"
    )
    print('[+]\t CONNECTED TO DATABASE')
    mycursor = mydb.cursor()
    sql = "UPDATE tb_scrapper SET status=1 WHERE tb_name='tb_saxo_3_25_delta'"
    mycursor.execute(sql)
    mydb.commit()
    
    try:
        cron_manager.saxo3_25_delta_cron_start()
    except Exception as e:
        print(e)
    
    return render_template('scrapper.html', async_mode=socketio.async_mode)

@app.route('/start_oanda')
def start_oanda():
    cron_manager = cron.Cron_scrapper()
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="forex_scraping"
    )
    print('[+]\t CONNECTED TO DATABASE')
    mycursor = mydb.cursor()
    sql = "UPDATE tb_scrapper SET status=1 WHERE tb_name='tb_oanda'"
    mycursor.execute(sql)
    mydb.commit()
    
    try:
        cron_manager.oanda_cron_start()
    except Exception as e:
        print(e)
    
    return render_template('scrapper.html', async_mode=socketio.async_mode)

@app.route('/stop')
def stop():
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="forex_scraping"
    )
    print('[+]\t CONNECTED TO DATABASE')
    mycursor = mydb.cursor()
    sql = "UPDATE tb_scrapper SET status=0"
    mycursor.execute(sql)
    mydb.commit()

    cron_manager = cron.Cron_scrapper()
    cron_manager.reset()
    return render_template('scrapper.html', async_mode=socketio.async_mode)

@app.route("/get_outlook_by_country/<id>")
def get_outlook_by_country(id):
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="forex_scraping"
    )
    print('[+]\t CONNECTED TO DATABASE')
    mycursor = mydb.cursor()

    outlook_id = id.split("_")
    pair_id = outlook_id[0]
    cycle_number = outlook_id[1]
    sql = "SELECT * FROM tb_myfxbook_by_country WHERE pair_id='"+pair_id+"' AND cycle_num='"+cycle_number+"'"
    mycursor.execute(sql)
    results=mycursor.fetchall()

    return render_template('country.html', outlook_by_country_data = results, name = outlook_id[2])

@socketio.on('connect')
def connect():
    global thread_xm
    global thread_dukascopy
    global thread_forexfactory
    global thread_scrapper
    global thread_tradecaptain
    global thread_dailyfx
    global thread_myfxbook
    global thread_saxo_1
    global thread_saxo_2
    global thread_saxo_3_atm
    global thread_saxo_3_25_delta
    global thread_oanda

    with thread_lock:
        if thread_xm is None:
            thread_xm = socketio.start_background_task(xm_background_thread)
        if thread_dukascopy is None:
            thread_dukascopy = socketio.start_background_task(dukascopy_background_thread)
        if thread_forexfactory is None:
            thread_forexfactory = socketio.start_background_task(forexfactory_background_thread)
        if thread_tradecaptain is None:
            thread_tradecaptain = socketio.start_background_task(tradecaptain_background_thread)
        if thread_dailyfx is None:
            thread_dailyfx = socketio.start_background_task(dailyfx_background_thread)
        if thread_myfxbook is None:
            thread_myfxbook = socketio.start_background_task(myfxbook_background_thread)
        if thread_saxo_1 is None:
            thread_saxo_1 = socketio.start_background_task(saxo_1_background_thread)
        if thread_saxo_2 is None:
            thread_saxo_2 = socketio.start_background_task(saxo_2_background_thread)
        if thread_saxo_3_atm is None:
            thread_saxo_3_atm = socketio.start_background_task(saxo_3_atm_background_thread) 
        if thread_saxo_3_25_delta is None:
            thread_saxo_3_25_delta = socketio.start_background_task(saxo_3_25_delta_background_thread) 
        if thread_oanda is None:
            thread_oanda = socketio.start_background_task(oanda_background_thread) 
        if thread_scrapper is None:
            thread_scrapper = socketio.start_background_task(get_scrapper_status_background_thread)

def xm_background_thread():
    while True:
        socketio.sleep(2)
        bot = XM.Xm_scrapper()
        bot.connect_mysql()
        forex_data_list = bot.get_forex_data()
        forex_data = []

        for data in forex_data_list:
            pair_data = {
                "pair"      : bot.get_pair_name(data[1]),
                "long"      : data[2],
                "short"     : data[3],
                "date"      : data[4],
                "time"      : data[5]
            }
            forex_data.append(pair_data)
            
        socketio.emit('forex_data_xm',
                      {'data': forex_data},
                      namespace='/xm_socket')

def dukascopy_background_thread():
    while True:
        socketio.sleep(2)
        bot = Dukascopy.Dukascopy_scrapper()
        bot.connect_mysql()
        forex_data_list = bot.get_forex_data()
        forex_data = []

        for data in forex_data_list:
            pair_data = {
                "pair"      : bot.get_pair_name(data[1]),
                "long"      : data[2],
                "short"     : data[3],
                "date"      : data[4],
                "time"      : data[5]
            }
            forex_data.append(pair_data)

        socketio.emit('forex_data_dukascopy',
                      {'data': forex_data},
                      namespace='/dukascopy_socket')

def tradecaptain_background_thread():
    while True:
        socketio.sleep(2)
        bot = Tradecaptain.Tradecaptain_scrapper()
        bot.connect_mysql()
        forex_data_list = bot.get_forex_data()
        forex_data = []
        
        for data in forex_data_list:
            pair_data = {
                "pair"      : bot.get_pair_name(data[1]),
                "long"      : data[2],
                "short"     : data[3],
                "date"      : data[4],
                "time"      : data[5]
            }
            forex_data.append(pair_data)

        socketio.emit('forex_data_tradecaptain',
                      {'data': forex_data},
                      namespace='/tradecaptain_socket')

def dailyfx_background_thread():
    while True:
        socketio.sleep(2)
        bot = Dailyfx.Dailyfx_scrapper()
        bot.connect_mysql()
        forex_data_list = bot.get_forex_data()
        forex_data = []
        
        for data in forex_data_list:
            pair_data = {
                "pair"      : bot.get_pair_name(data[1]),
                "long"      : data[2],
                "short"     : data[3],
                "date"      : data[4],
                "time"      : data[5]
            }
            forex_data.append(pair_data)

        socketio.emit('forex_data_dailyfx',
                      {'data': forex_data},
                      namespace='/dailyfx_socket')

def myfxbook_background_thread():
    while True:
        socketio.sleep(2)
        bot = Myfxbook.Myfxbook_scrapper()
        bot.connect_mysql()
        forex_data_list = bot.get_forex_data()
        forex_data = []
        
        for data in forex_data_list:
            pair_data = {
                "name"                  : bot.get_pair_name(data[1]),
                "shortPercentage"       : data[2],
                "longPercentage"        : data[3],
                "shortVolume"           : data[4],
                "longVolume"            : data[5],
                "longPositions"         : data[6],
                "shortPositions"        : data[7],
                "totalPositions"        : data[8],
                "avgShortPrice"         : data[9],
                "avgLongPrice"          : data[10],
                "cycle_num"             : data[11],
                "date"                  : data[12],
                "time"                  : data[13],
                "id"                    : data[1]
            }
            forex_data.append(pair_data)

        socketio.emit('forex_data_myfxbook',
                      {'data': forex_data},
                      namespace='/myfxbook_socket')

def forexfactory_background_thread():
    while True:
        socketio.sleep(2)
        bot = Forexfactory.Forexfactory_scrapper()
        bot.connect_mysql()
        forex_data_list = bot.get_forex_data()
        forex_data = []

        for data in forex_data_list:
            pair_data = {
                "date"                  : data[1],
                "time"                  : data[2],
                "instrument"            : bot.get_pair_name(data[3]),
                "long_traders"          : data[4],
                "short_traders"         : data[5],
                "long_traders_ratio"    : data[6],
                "short_traders_ratio"   : data[7],
                "long_positions"        : data[8],
                "short_positions"       : data[9],
                "long_positions_ratio"  : data[10],
                "short_positions_ratio" : data[11],
            }
            forex_data.append(pair_data)

        socketio.emit('forex_data_forexfactory',
                      {'data': forex_data},
                      namespace='/forexfactory_socket')
                    
def saxo_1_background_thread():
    while True:
        socketio.sleep(2)
        bot = Saxo_1.Saxo_1_scopy_scrapper()
        bot.connect_mysql()
        forex_data_list = bot.get_forex_data()
        forex_data = []
        
        for data in forex_data_list:
            pair_data = {
                "pair"      : bot.get_pair_name(data[1]),
                "long"      : data[2],
                "short"     : data[3],
                "date"      : data[4],
                "time"      : data[5]
            }
            forex_data.append(pair_data)

        socketio.emit('forex_data_saxo_1',
                      {'data': forex_data},
                      namespace='/saxo_1_socket')
                    
def saxo_2_background_thread():
    while True:
        socketio.sleep(2)
        bot = Saxo_2.Saxo_2_scrapper()
        bot.connect_mysql()
        forex_data_list = bot.get_forex_data()
        forex_data = []
        
        for data in forex_data_list:
            pair_data = {
                "pair"      : bot.get_pair_name(data[1]),
                "long"      : data[2],
                "short"     : data[3],
                "date"      : data[4],
                "time"      : data[5]
            }
            forex_data.append(pair_data)

        socketio.emit('forex_data_saxo_2',
                      {'data': forex_data},
                      namespace='/saxo_2_socket')

def saxo_3_atm_background_thread():
    while True:
        socketio.sleep(2)
        bot = Saxo_3_atm.Saxo_3_atm_scrapper()
        bot.connect_mysql()
        forex_data_list = bot.get_forex_data()
        forex_data = []
        
        for data in forex_data_list:
            pair_data = {
                "pair"      : bot.get_pair_name(data[1]),
                "spot"      : data[2],
                "1w"        : data[3],
                "1m"        : data[4],
                "3m"        : data[5],
                "6m"        : data[6],
                "9m"        : data[7],
                "1y"        : data[8],
                "date"      : data[9],
                "time"      : data[10]
            }
            forex_data.append(pair_data)

        socketio.emit('forex_data_saxo_3_atm',
                      {'data': forex_data},
                      namespace='/saxo_3_atm_socket')

def saxo_3_25_delta_background_thread():
    while True:
        socketio.sleep(2)
        bot = Saxo_3_25_delta.Saxo_3_25_delta_scrapper()
        bot.connect_mysql()
        forex_data_list = bot.get_forex_data()
        forex_data = []
        
        for data in forex_data_list:
            pair_data = {
                "pair"      : bot.get_pair_name(data[1]),
                "spot"      : data[2],
                "1w"        : data[3],
                "1m"        : data[4],
                "3m"        : data[5],
                "6m"        : data[6],
                "9m"        : data[7],
                "1y"        : data[8],
                "date"      : data[9],
                "time"      : data[10]
            }
            forex_data.append(pair_data)

        socketio.emit('forex_data_saxo_3_25_delta',
                      {'data': forex_data},
                      namespace='/saxo_3_25_delta_socket')
                    
def oanda_background_thread():
    while True:
        socketio.sleep(2)
        bot = Oanda.Oanda_scrapper()
        bot.connect_mysql()
        forex_data_list = bot.get_forex_data()
        forex_data = []
        
        for data in forex_data_list:
            pair_data = {
                "pair"      : bot.get_pair_name(data[1]),
                "long"      : data[2],
                "short"     : data[3],
                "date"      : data[4],
                "time"      : data[5]
            }
            forex_data.append(pair_data)

        socketio.emit('forex_data_oanda',
                      {'data': forex_data},
                      namespace='/oanda_socket')

def get_scrapper_status_background_thread():
    while True:
        socketio.sleep(2)
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="root",
            database="forex_scraping"
        )
        print('[+]\t CONNECTED TO DATABASE')
        mycursor = mydb.cursor()
        sql = "SELECT * FROM tb_scrapper"
        mycursor.execute(sql)
        results=mycursor.fetchall()
        scrapper_data = []

        for result in results:
            result = {
                "name"       : result[1],
                "frequency"  : result[4],
                "status"     : result[5]
            }
            scrapper_data.append(result)

        socketio.emit('scrapper_data',
                      {'data': scrapper_data},
                      namespace='/scrapper_data_socket')

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0')