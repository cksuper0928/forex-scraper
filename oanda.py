from scrappers import scrapper_oanda as Oanda
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoAlertPresentException, NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.remote.webdriver import WebDriver as RemoteWebDriver
import time
import random
import os
from multiprocessing import Process
import sys
import requests
from datetime import date, timedelta, datetime

oanda_url = "https://trade.oanda.com/labs/position-ratios/?embedded=true&useDefaultPracticeToken=e724aeb6b016050360caeab621405665-7c686510c4e9ac37138488e339da7007"

bot = Oanda.Oanda_scrapper()
bot.connect_mysql()
bot.get_status()



# Configure Chrome Web Driver
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("no-sandbox")
options.add_argument("--disable-extensions")

driver = webdriver.Chrome("/usr/local/bin/chromedriver", options=options)
driver.set_page_load_timeout("10")
driver.set_window_size(1341,810)

try:
    driver.get(oanda_url)
except Exception as e:
    print(e)

time.sleep(5)

pair_list = []
pairs = driver.find_elements_by_xpath(".//div[@class='flex-container position-long-short-row style-scope position-ratios']")

now = datetime.now()
d_time=now.strftime("%Y-%m-%d %H:%M:%S")
d_time_date = now.strftime("%d/%m/%Y")
d_time_time = now.strftime("%H:%M:%S")

for pair in pairs:
    pair_name = pair.find_element_by_xpath(".//div[@class='instrument style-scope position-ratios']").text
    pair_name = pair_name.replace("/", "")
    pair_long = pair.find_element_by_xpath(".//div[@class='position long-position style-scope position-ratios']").text
    pair_long = pair_long.replace("%", "")
    pair_short = pair.find_element_by_xpath(".//div[@class='position short-position style-scope position-ratios']").text
    pair_short = pair_short.replace("%", "")
    pair_info = {
        "pair"      :   pair_name,
        "long"      :   pair_long,
        "short"     :   pair_short,
        "date"      :   d_time_date,
        "time"      :   d_time_time,
    }
    pair_list.append(pair_info)

# print(pair_list)
bot.insert_pairs(pair_list)

driver.close()