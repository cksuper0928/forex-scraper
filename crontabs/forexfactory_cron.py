import os.path
from crontab import CronTab

forexfactory_tab = CronTab(tabfile=os.path.dirname(__file__)+'/../forexfactory_cron.tab')
for result in forexfactory_tab.run_scheduler():
    print("Forexfactory Cron is running")