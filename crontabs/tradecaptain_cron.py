import os.path
from crontab import CronTab

tradecaptain_tab = CronTab(tabfile=os.path.dirname(__file__)+'/../tradecaptain_cron.tab')
for result in tradecaptain_tab.run_scheduler():
    print("Tradecaptain Cron is running")