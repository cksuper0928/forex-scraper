import os.path
from crontab import CronTab

dailyfx_tab = CronTab(tabfile=os.path.dirname(__file__)+'/../dailyfx_cron.tab')
for result in dailyfx_tab.run_scheduler():
    print("Dailyfx Cron is running")