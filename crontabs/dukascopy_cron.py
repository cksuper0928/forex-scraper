import os.path
from crontab import CronTab

dukascopy_tab = CronTab(tabfile=os.path.dirname(__file__)+'/../dukascopy_cron.tab')
for result in dukascopy_tab.run_scheduler():
    print("Dukascopy Cron is running")