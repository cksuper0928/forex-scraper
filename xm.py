from scrappers import scrapper_xm as XM 

xm_url = "https://www.xm.com/"

# xm.com
bot = XM.Xm_scrapper()
bot.connect_mysql()
bot.get_status()
page_soap = bot.fetch_soup(xm_url)
pairs = bot.get_pairs(page_soap)
bot.insert_pairs(pairs)

# from datetime import datetime
# myFile = open('append.txt', 'a') 
# myFile.write('\nAccessed on ' + str(datetime.now()))