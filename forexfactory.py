from scrappers import scrapper_forexfactory as Forexfactory 
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoAlertPresentException, NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.remote.webdriver import WebDriver as RemoteWebDriver
import time
import random
import os
from multiprocessing import Process
import sys
import requests
from datetime import date, timedelta, datetime

bot = Forexfactory.Forexfactory_scrapper()
bot.connect_mysql()
bot.get_status()

# Configure Chrome Web Driver
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("no-sandbox")
options.add_argument("--disable-extensions")

driver = webdriver.Chrome("/usr/local/bin/chromedriver", options=options)
driver.set_page_load_timeout("10")
driver.set_window_size(1341,810)

try:
    driver.get("https://www.forexfactory.com/#tradesPositions")
except Exception as e:
    print(e)

time.sleep(5)

# Apply Settings to display traders and lots
settings_strong = driver.find_element_by_xpath(".//strong[text()='Positions / Live Accounts']")
settings_button_span = settings_strong.find_element_by_xpath("..")
settings_button = settings_button_span.find_element_by_xpath("..")
driver.execute_script("arguments[0].click();", settings_button)
time.sleep(2)
driver.find_element_by_xpath(".//label[text()='Traders and Lots']").click()
time.sleep(2)
detail_div = driver.find_element_by_xpath(".//div[@data-compid='TradePositions_Copy1']")
detail_div.find_element_by_xpath(".//input[@value='Apply Settings']").click()
time.sleep(4)

# Click Expand button
try:
    expand_span = driver.find_element_by_xpath(".//span[text()='Expand']")
    expand_button = expand_span.find_element_by_xpath("..")
    driver.execute_script("arguments[0].click();", expand_button)
    time.sleep(2)
except Exception as e:
    print(e)

# Open Details
try:
    detail_div = driver.find_element_by_xpath(".//div[@data-compid='TradePositions_Copy1']")
    detail_buttons = detail_div.find_elements_by_xpath(".//a[@title='Open Detail']")
    time.sleep(2)
except Exception as e:
    print(e)

# Click Open Details
for button in detail_buttons:
    try:
        driver.execute_script("arguments[0].click();", button)
    except Exception as e:
        print(e)

# Find forex divs
time.sleep(5)
try:
    forex_divs = detail_div.find_elements_by_xpath(".//div[@class='tradespositiondetail__contents']")
except Exception as e:
    print(e)

# Find forex tables
time.sleep(10)
try:
    forex_tables = detail_div.find_elements_by_xpath(".//table[@class='trades_position']")
except Exception as e:
    print(e)


# Get required fields
forex_data = []
now = datetime.now()
d_date=now.strftime("%d/%m/%Y")
d_time=now.strftime("%H:%M:%S")

for x in range(len(forex_tables)):
    instrument = forex_tables[x].find_element_by_xpath(".//a[@class='currency']").text

    trade_tb = forex_tables[x].find_element_by_xpath(".//div[@class='trades_position__chart trades_position__chart--traders traders']")
    traders_ratio = trade_tb.find_elements_by_xpath(".//strong")
    long_traders_ratio = traders_ratio[0].text
    short_traders_ratio = traders_ratio[1].text

    long_traders = traders_ratio[0].find_element_by_xpath("..").text
    short_traders = traders_ratio[1].find_element_by_xpath("..").text
    long_traders = long_traders.replace(long_traders_ratio, "")
    short_traders = short_traders.replace(short_traders_ratio, "")

    trade_tb = forex_tables[x].find_element_by_xpath(".//div[@class='trades_position__chart trades_position__chart--lots lots']")
    lots_ratio = trade_tb.find_elements_by_xpath(".//strong")
    long_lots_ratio = lots_ratio[0].text
    short_lots_ratio = lots_ratio[1].text

    long_lots = lots_ratio[0].find_element_by_xpath("..").text
    short_lots = lots_ratio[1].find_element_by_xpath("..").text
    long_lots = long_lots.replace(long_lots_ratio, "")
    short_lots = short_lots.replace(short_lots_ratio, "")

    long_positions = long_lots
    long_positions_ratio = long_lots_ratio
    short_positions = short_lots
    short_positions_ratio = short_lots_ratio

    instrument = instrument.replace("/", "")
    long_traders = long_traders.replace("Traders", "")
    short_traders = short_traders.replace("Traders", "")
    long_traders_ratio = long_traders_ratio.replace("%", "")
    short_traders_ratio = short_traders_ratio.replace("%", "")
    long_positions = long_positions.replace("Lots", "")
    long_positions_ratio = long_positions_ratio.replace("%", "")
    short_positions = short_positions.replace("Lots", "")
    short_positions_ratio = short_positions_ratio.replace("%", "")

    

    data = {
        "instrument" : instrument,
        "long_traders" : long_traders,
        "short_traders" : short_traders,
        "long_traders_ratio" : long_traders_ratio,
        "short_traders_ratio" : short_traders_ratio,
        "long_positions" : long_positions,
        "long_positions_ratio" : long_positions_ratio,
        "short_positions" : short_positions,
        "short_positions_ratio" : short_positions_ratio,
        "date" : d_date,
        "time" : d_time
    }

    forex_data.append(data)

driver.close()
# print(forex_data)

bot.insert_pairs(forex_data)