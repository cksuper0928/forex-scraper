from scrappers import scrapper_saxo2 as Saxo_2

saxo_2_url = "https://fxowebtools.saxobank.com/retail.html"

bot = Saxo_2.Saxo_2_scrapper()
bot.connect_mysql()
bot.get_status()
page_soap = bot.fetch_soup(saxo_2_url)
pairs = bot.get_pairs(page_soap)
bot.insert_pairs(pairs)