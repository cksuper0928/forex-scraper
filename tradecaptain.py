from scrappers import scrapper_tradecaptain as Tradecaptain 
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoAlertPresentException, NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.remote.webdriver import WebDriver as RemoteWebDriver
import time
import random
import os
from multiprocessing import Process
import sys
import requests
from datetime import date, timedelta, datetime


tradecaptain_url = "https://prices.tradecaptain.com/ar/widgets/all-sentiments"

# xm.com
bot = Tradecaptain.Tradecaptain_scrapper()
bot.connect_mysql()
bot.get_status()
page_soap = bot.fetch_soup(tradecaptain_url)
pairs = bot.get_pairs(page_soap)
bot.insert_pairs(pairs)
