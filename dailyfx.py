from scrappers import scrapper_dailyfx as Dailyfx
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoAlertPresentException, NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.remote.webdriver import WebDriver as RemoteWebDriver
import time
import random
import os
from multiprocessing import Process
import sys
import requests
from datetime import date, timedelta, datetime

dailyfx_url = "https://www.dailyfx.com/sentiment"

bot = Dailyfx.Dailyfx_scrapper()
bot.connect_mysql()
bot.get_status()

pair_list = []

# Configure Chrome Web Driver
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("no-sandbox")
options.add_argument("--disable-extensions")

driver = webdriver.Chrome("/usr/local/bin/chromedriver", options=options)
driver.set_page_load_timeout("10")
driver.set_window_size(1341,810)

try:
    driver.get(dailyfx_url)
except Exception as e:
    print(e)

time.sleep(8)

try:
    divs = driver.find_elements_by_xpath(".//div[contains(@class,'dfx-technicalSentimentCard  text-black')]")
except Exception as e:
    print(e)

datetime = driver.find_element_by_xpath(".//span[@class='dfx-technicalSentimentPageTopBar__lastUpdatedDate']").get_attribute("data-time")
datetime_split = datetime.split("T")

date = datetime_split[0]
date_split = date.split("-")
date = date_split[2]+"/"+date_split[1]+"/"+date_split[0]

time = datetime_split[1]
time = time.replace("+00:00", "")

for pair_div in divs:
    pair_name = pair_div.find_element_by_xpath(".//a[@class='dfx-technicalSentimentCard__pair dfx-technicalSentimentCard__pair--link text-uppercase text-black']").text
    pair_name = pair_name.replace("/", "")
    long_div = pair_div.find_element_by_xpath(".//div[@class='dfx-technicalSentimentCard__netLongContainer']")
    pair_long = long_div.find_element_by_xpath(".//span[@class='dfx-rateDetail__percentageInfoText font-weight-bold']").get_attribute("data-value")
    short_div = pair_div.find_element_by_xpath(".//div[@class='dfx-technicalSentimentCard__netShortContainer']")
    pair_short = short_div.find_element_by_xpath(".//span[@class='dfx-rateDetail__percentageInfoText font-weight-bold']").get_attribute("data-value")
    pair_info = {
        "pair"      :   pair_name,
        "long"      :   pair_long,
        "short"     :   pair_short,
        "date"      :   date,
        "time"      :   time
    }
    pair_list.append(pair_info)

driver.close()
bot.insert_pairs(pair_list)