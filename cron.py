from scrappers import scrapper_xm as XM
from scrappers import scrapper_dukascorpy as Dukascopy
from scrappers import scrapper_forexfactory as Forexfactory
from scrappers import scrapper_tradecaptain as Tradecaptain
from scrappers import scrapper_dailyfx as Dailyfx
from scrappers import scrapper_myfxbook as Myfxbook
from scrappers import scrapper_saxo1 as Saxo_1
from scrappers import scrapper_saxo2 as Saxo_2
from scrappers import scrapper_saxo_3_atm as Saxo_3_atm
from scrappers import scrapper_saxo_3_25_delta as Saxo_3_25_delta
from scrappers import scrapper_oanda as Oanda
# import xm as XM_Manager
import os
from crontab import CronTab

class Cron_scrapper():
    def __init__(self):
        self.xm_bot = XM.Xm_scrapper()
        self.dukascopy_bot = Dukascopy.Dukascopy_scrapper()
        self.forexfactory_bot = Forexfactory.Forexfactory_scrapper()
        self.tradecaptain_bot = Tradecaptain.Tradecaptain_scrapper()
        self.dailyfx_bot = Dailyfx.Dailyfx_scrapper()
        self.myfxbook_bot = Myfxbook.Myfxbook_scrapper()
        self.saxo_1_bot = Saxo_1.Saxo_1_scopy_scrapper()
        self.saxo_2_bot = Saxo_2.Saxo_2_scrapper()
        self.saxo_3_atm_bot = Saxo_3_atm.Saxo_3_atm_scrapper()
        self.saxo_3_25_delta_bot = Saxo_3_25_delta.Saxo_3_25_delta_scrapper()
        self.oanda_bot = Oanda.Oanda_scrapper()
        self.cron   = CronTab(user='root')
    
    def xm_cron_start(self):
        self.xm_bot.connect_mysql()
        self.xm_bot.get_status()
        xm_status = self.xm_bot.status
        xm_frequecy = xm_status["frequency"]
        interval = int(xm_frequecy/60)
        xm_file_path = os.path.dirname(os.path.abspath(__file__))
        xm_command = "python3 " + xm_file_path +"/xm.py"
        # xm_command = "python3 xm.py"
        
        self.xm_job = self.cron.new(command=xm_command)
        self.xm_job.minute.every(interval)
        self.cron.write()
    
    def dukascopy_cron_start(self):
        self.dukascopy_bot.connect_mysql()
        self.dukascopy_bot.get_status()
        dukascopy_status = self.dukascopy_bot.status
        dukascopy_frequecy = dukascopy_status["frequency"]
        interval = int(dukascopy_frequecy/60)
        dukascopy_file_path = os.path.dirname(os.path.abspath(__file__))
        dukascopy_command = "python3 " + dukascopy_file_path +"/dukascopy.py"
        # dukascopy_command = "python3 dukascopy.py"
        
        self.dukascopy_job = self.cron.new(command=dukascopy_command)
        self.dukascopy_job.minute.every(interval)
        self.cron.write()
    
    def forexfactory_cron_start(self):
        self.forexfactory_bot.connect_mysql()
        self.forexfactory_bot.get_status()
        forexfactory_status = self.forexfactory_bot.status
        forexfactory_frequecy = forexfactory_status["frequency"]
        interval = int(forexfactory_frequecy/60)
        forexfactory_file_path = os.path.dirname(os.path.abspath(__file__))
        forexfactory_command = "python3 " + forexfactory_file_path +"/forexfactory.py"
        # forexfactory_command = "python3 forexfactory.py"
        
        self.forexfactory_job = self.cron.new(command=forexfactory_command)
        self.forexfactory_job.minute.every(interval)
        self.cron.write()

    def tradecaptain_cron_start(self):
        self.tradecaptain_bot.connect_mysql()
        self.tradecaptain_bot.get_status()
        tradecaptain_status = self.tradecaptain_bot.status
        tradecaptain_frequecy = tradecaptain_status["frequency"]
        interval = int(tradecaptain_frequecy/60)
        tradecaptain_file_path = os.path.dirname(os.path.abspath(__file__))
        tradecaptain_command = "python3 " + tradecaptain_file_path +"/tradecaptain.py"
        # tradecaptain_command = "python3 tradecaptain.py"
        
        self.tradecaptain_job = self.cron.new(command=tradecaptain_command)
        self.tradecaptain_job.minute.every(interval)
        self.cron.write()
    
    def dailyfx_cron_start(self):
        self.dailyfx_bot.connect_mysql()
        self.dailyfx_bot.get_status()
        dailyfx_status = self.dailyfx_bot.status
        dailyfx_frequecy = dailyfx_status["frequency"]
        interval = int(dailyfx_frequecy/60)
        # dailyfx_command = "python3 dailyfx.py"
        dailyfx_file_path = os.path.dirname(os.path.abspath(__file__))
        dailyfx_command = "python3 " + dailyfx_file_path +"/dailyfx.py"
        
        self.dailyfx_job = self.cron.new(command=dailyfx_command)
        self.dailyfx_job.minute.every(interval)
        self.cron.write()

    def myfxbook_cron_start(self):
        self.myfxbook_bot.connect_mysql()
        self.myfxbook_bot.get_status()
        myfxbook_status = self.myfxbook_bot.status
        myfxbook_frequecy = myfxbook_status["frequency"]
        interval = int(myfxbook_frequecy/60)
        myfxbook_file_path = os.path.dirname(os.path.abspath(__file__))
        myfxbook_command = "python3 " + myfxbook_file_path +"/myfxbook.py"
        
        self.myfxbook_job = self.cron.new(command=myfxbook_command)
        self.myfxbook_job.minute.every(interval)
        self.cron.write()
    
    def saxo1_cron_start(self):
        self.saxo_1_bot.connect_mysql()
        self.saxo_1_bot.get_status()
        saxo_1_status = self.saxo_1_bot.status
        saxo_1_frequecy = saxo_1_status["frequency"]
        interval = int(saxo_1_frequecy/60)
        saxo_1_file_path = os.path.dirname(os.path.abspath(__file__))
        saxo_1_command = "python3 " + saxo_1_file_path +"/saxo_1.py"
        
        self.saxo_1_job = self.cron.new(command=saxo_1_command)
        self.saxo_1_job.minute.every(interval)
        self.cron.write()
    
    def saxo2_cron_start(self):
        self.saxo_2_bot.connect_mysql()
        self.saxo_2_bot.get_status()
        saxo_2_status = self.saxo_2_bot.status
        saxo_2_frequecy = saxo_2_status["frequency"]
        interval = int(saxo_2_frequecy/60)
        saxo_2_file_path = os.path.dirname(os.path.abspath(__file__))
        saxo_2_command = "python3 " + saxo_2_file_path +"/saxo_2.py"
        
        self.saxo_2_job = self.cron.new(command=saxo_2_command)
        # self.saxo_2_job.minute.every(interval)
        self.saxo_2_job.day.every(1)
        self.saxo_2_job.hour.on(7)
        self.saxo_2_job.minute.on(30)
        self.cron.write()
    
    def saxo3_atm_cron_start(self):
        self.saxo_3_atm_bot.connect_mysql()
        self.saxo_3_atm_bot.get_status()
        saxo_3_atm_status = self.saxo_3_atm_bot.status
        saxo_3_atm_frequecy = saxo_3_atm_status["frequency"]
        interval = int(saxo_3_atm_frequecy/60)
        saxo_3_atm_file_path = os.path.dirname(os.path.abspath(__file__))
        saxo_3_atm_command = "python3 " + saxo_3_atm_file_path +"/saxo_3_atm.py"
        
        self.saxo_3_atm_job = self.cron.new(command=saxo_3_atm_command)
        # self.saxo_3_atm_job.minute.every(interval)
        self.saxo_3_atm_job.hour.every(1)
        self.cron.write()
    
    def saxo3_25_delta_cron_start(self):
        self.saxo_3_25_delta_bot.connect_mysql()
        self.saxo_3_25_delta_bot.get_status()
        saxo_3_25_delta_status = self.saxo_3_25_delta_bot.status
        saxo_3_25_delta_frequecy = saxo_3_25_delta_status["frequency"]
        interval = int(saxo_3_25_delta_frequecy/60)
        saxo_3_25_delta_file_path = os.path.dirname(os.path.abspath(__file__))
        saxo_3_25_delta_command = "python3 " + saxo_3_25_delta_file_path +"/saxo_3_25_delta.py"
        
        self.saxo_3_25_delta_job = self.cron.new(command=saxo_3_25_delta_command)
        # self.saxo_3_25_delta_job.minute.every(interval)
        self.saxo_3_25_delta_job.hour.every(1)
        self.cron.write()
    
    def oanda_cron_start(self):
        self.oanda_bot.connect_mysql()
        self.oanda_bot.get_status()
        oanda_status = self.oanda_bot.status
        oanda_frequecy = oanda_status["frequency"]
        interval = int(oanda_frequecy/60)
        oanda_file_path = os.path.dirname(os.path.abspath(__file__))
        oanda_command = "python3 " + oanda_file_path +"/oanda.py"
        
        self.oanda_job = self.cron.new(command=oanda_command)
        self.oanda_job.minute.every(interval)
        self.cron.write()
    
    def kill_process(self):
        stop_job = self.cron.new(command='reboot')
        stop_job.run()
    
    def reset(self):
        self.cron.remove_all()
        self.cron.write()