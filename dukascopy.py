from scrappers import scrapper_dukascorpy as Dukascorpy 

url = "https://freeserv.dukascopy.com/2.0/api/"
params = {
    'group':'quotes',
    'method': 'realtimeSentimentIndex',
    'enabled': 'true',
    'key':'odkh5fclm8000000',
    'type':'swfx'
}

bot = Dukascorpy.Dukascopy_scrapper()
bot.connect_mysql()
bot.get_status()
pairs = bot.get_pairs(params, url)
bot.insert_pairs(pairs)